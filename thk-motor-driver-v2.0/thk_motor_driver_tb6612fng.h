/*
 * Klasse: thk_MotorDriverTB6612FNG
 *
 * Zur Ansteuerung von Gleichstrommotoren mit dem Motortreiber TB6612FNG
 * 
 * S. Landwein, Januar 2022
 * 
 */

#ifndef MOTOR_DRIVER_TB6612FNG_H
#define MOTOR_DRIVER_TB6612FNG_H

#include <Arduino.h>
#include "thk_motor_driver.h"

class thk_MotorDriverTB6612FNG : public thk_MotorDriver
{
    public:
        thk_MotorDriverTB6612FNG(uint8_t motor_count, uint8_t pin_stby, uint8_t pin_pwm_a, uint8_t pin_in_1, uint8_t pin_in_2, uint8_t pin_pwm_b, uint8_t pin_in_3, uint8_t pin_in_4);
        void stop() override; 
        void drive(uint8_t speed, bool is_forward) override;   
        void init();

    private:
        uint8_t motor_count;
        uint8_t pin_stby;
        uint8_t pin_pwm_a;
        uint8_t pin_in_1;
        uint8_t pin_in_2;
        uint8_t pin_pwm_b; 
        uint8_t pin_in_3; 
        uint8_t pin_in_4; 
};

#endif