#include "thk_motor_driver.h"

void thk_MotorDriver::drive_forward(uint8_t speed) {
    this->drive(speed, true);
}

void thk_MotorDriver::drive_backward(uint8_t speed) {
    this->drive(speed, false);
}