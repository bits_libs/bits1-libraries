/*
*   Interface: thk_Motortreiber
*   
*   Enthält alle notwendigen Standardfunktionen eines Motortreibers
*   
*   S.Landwein, April 2022
*   
*/


#ifndef MOTORDRIVER_H
#define MOTORDRIVER_H

#include <Arduino.h>

class thk_MotorDriver {
    public:
        enum Driving_state { stoped, forward, backward};
        void drive_forward(uint8_t speed);
        void drive_backward(uint8_t speed);
        virtual void stop() = 0;
        virtual void drive(uint8_t speed, bool is_forward) = 0;

    protected:
        Driving_state driving_state = stoped;
        uint8_t speed = 0;
        bool is_setup_correct = false;  
};

#endif