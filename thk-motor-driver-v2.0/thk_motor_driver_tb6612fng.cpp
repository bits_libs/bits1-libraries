#include "thk_motor_driver_tb6612fng.h"

thk_MotorDriverTB6612FNG::thk_MotorDriverTB6612FNG(uint8_t motor_count, uint8_t pin_stby, uint8_t pin_pwm_a, uint8_t pin_in_1, uint8_t pin_in_2, uint8_t pin_pwm_b, uint8_t pin_in_3, uint8_t pin_in_4) : motor_count(motor_count), pin_stby(pin_stby), pin_pwm_a(pin_pwm_a),  pin_in_1(pin_in_1), pin_in_2(pin_in_2), pin_pwm_b(pin_pwm_b), pin_in_3(pin_in_3), pin_in_4(pin_in_4){}

void thk_MotorDriverTB6612FNG::init()
{
    if(this->motor_count < 1 || this->motor_count > 2)
    {
        this->is_setup_correct = false;
        Serial.println("Ungültige Anzahl an Motoren:");
        Serial.println(this->motor_count);
        return;
    }

    pinMode(this->pin_stby, OUTPUT);
    pinMode(this->pin_pwm_a, OUTPUT);
    pinMode(this->pin_in_1, OUTPUT);
    pinMode(this->pin_in_2, OUTPUT);
    digitalWrite(this->pin_in_1, LOW);
    digitalWrite(this->pin_in_2, LOW);

    if (this->motor_count == 2)
    {
        if ((pin_in_3 == 0) || (pin_in_4 == 0) || (pin_pwm_b == 0)){
            this->is_setup_correct = false;
            Serial.println("Ungültige Pinbelegung");
            return;
        }

        pinMode(this->pin_pwm_b, OUTPUT);
        pinMode(this->pin_in_3, OUTPUT);
        pinMode(this->pin_in_4, OUTPUT);
        digitalWrite(this->pin_in_3, LOW);
        digitalWrite(this->pin_in_4, LOW);
    }

    this->is_setup_correct = true;
}

void thk_MotorDriverTB6612FNG::drive(byte speed, bool is_forward) {
    if(this->is_setup_correct == false)
    {
        Serial.println("Motoren werden nicht angesteuert, da keine gültige Initialisierung durchgeführt worden ist!");
        return;
    }

    if (is_forward && driving_state == backward) {
        stop();
        delay(500);
        driving_state = forward;
    }
    else if (!is_forward && driving_state == forward)
    {
        stop();
        delay(500);
        driving_state = backward;
    }

    digitalWrite(this->pin_stby, HIGH);
    analogWrite(this->pin_pwm_a, speed);

    if (is_forward) {
        digitalWrite(this->pin_in_1, LOW);
        digitalWrite(this->pin_in_2, HIGH);
    }
    else
    { 
        digitalWrite(this->pin_in_1, HIGH); 
        digitalWrite(this->pin_in_2, LOW);
    }

    if (this->motor_count == 2)
    {
        analogWrite(this->pin_pwm_b, speed); 

        if (is_forward) {
            digitalWrite(this->pin_in_3, LOW); 
            digitalWrite(this->pin_in_4, HIGH);
        }
        else
        { 
            digitalWrite(this->pin_in_3, HIGH); 
            digitalWrite(this->pin_in_4, LOW);
        }
    }
}

void thk_MotorDriverTB6612FNG::stop() {
    if(this->is_setup_correct == false)
    {
        Serial.println("Motoren werden nicht angesteuert, da keine gültige Initialisierung durchgeführt worden ist!");
        return;
    }

    digitalWrite(this->pin_stby, HIGH);
    digitalWrite(this->pin_in_1, HIGH); 
    digitalWrite(this->pin_in_2, HIGH);

    if(this->motor_count == 2) {
        digitalWrite(this->pin_in_3, HIGH); 
        digitalWrite(this->pin_in_4, HIGH);
    }
    driving_state = stoped;
}