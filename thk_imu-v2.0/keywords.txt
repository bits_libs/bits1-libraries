#######################################
# Datatypes (KEYWORD1)
#######################################

IMU                            KEYWORD1


#######################################
# Methods and Functions (KEYWORD2)
#######################################

init                           KEYWORD2
reset                          KEYWORD2
get_quat_w                     KEYWORD2
get_quat_x                     KEYWORD2
get_quat_y                     KEYWORD2
get_quat_z                     KEYWORD2
get_yaw                        KEYWORD2
get_pitch                      KEYWORD2
get_roll                       KEYWORD2
get_orientation                KEYWORD2