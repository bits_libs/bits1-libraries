# **IMU**

Eine Klasse für die leichte Initializierung und Ansteuerung des Beschleunigungssensors MPU6050.

Getestet mit:

- Arduino Mega Pro + IMU (GY-521) MPU6050<br />
<br />

## **Voraussetzung**
- [MPU6050](https://github.com/electroniccats/mpu6050) von ElectronicCats<br />
<br />

## **Installation:**

Um diese Klassen verwenden zu können, muss dieses Repository geklont und in das Libraries-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung:**

Zur Verwendung siehe zunächst das Beispiel `read_values.ino`<br />
<br />

**Einbinden der Bibliothek:**

```arduino
#include <thk_imu.h>
```

**Instanziieren:**

```arduino
const int INTERRUPT_PIN = 0;   // Wenn kein Interrupt Pin angeschlossen ist, dann Interrupt Pin gleich 0 setzen.
thk_IMU imu(INTERRUPT_PIN);
```

**Funktionen:**

Um die IMU zu initialisieren wird folgende Methode im setup ausgeführt:
```arduino
imu.init()
```

Um die sogenannten Yaw, Pitch und Roll Winkel seperat zu erhalten werden folgende Methoden ausgeführt (Je nach Einbau-Orientierung des Sensors, können Yaw, Pitch und Roll vertauscht sein):
```arduino
yaw = imu.get_z_rot();
pitch = imu.get_y_rot();
roll = imu.get_x_rot();
```

Möchte man sich die Yaw, Pitch und Roll Winkel gemeinsam zurück geben lassen, wird folgende Methode benutzt:
```arduino
yaw, pitch, roll = imu.get_orientation()
```

Um die sogenannten Quarternionen zu erhalten werden folgende Methoden ausgeführt:
```arduino
quat_w = imu.get_quat_w();
quat_x = imu.get_quat_x();
quat_y = imu.get_quat_y();
quat_z = imu.get_quat_z();
```

Um die Ausrichtung der IMU zu aktualisieren führt man folgende Methode aus:
```arduino
imu.reset()
```
